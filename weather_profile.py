import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import calendar
from datetime import datetime


class WeatherPlot:
    """Visualisation weather data (precipitation) from dataframe"""
    UNNECESSARY_ROWS = 9
    MONTHS_IN_YEAR = 12
    YEAR = 2019

    def clean_data(self, input_data):
        """ Clean input database from unnecessary data and convert date in correct format"""
        df_head = input_data.drop(input_data.index[0:self.UNNECESSARY_ROWS])
        df_head.index = range(len(df_head))
        new_header = ["Date", "Max_temperture", "Min_temperature", "Mean_temperature", "Precipitation level"]
        df_head.columns = new_header

        date_input = df_head["Date"]
        date = [0] * len(date_input)
        year = [0] * len(date_input)
        month = [0] * len(date_input)
        day = [0] * len(date_input)
        day_of_year = [0] * len(date_input)

        for item in range(len(date_input)):
            date[item] = datetime.strptime(date_input[item].split("T")[0], '%Y%m%d')
            year[item]=date[item].year
            month[item] = date[item].month
            day[item] = date[item].day
            day_of_year[item] = date[item].strftime('%j')

        df_head = df_head.assign(Year=year, Month=month, Day=day, Day_of_year=day_of_year)
        df_head = df_head.drop(columns=['Date', "Max_temperture", "Min_temperature", "Mean_temperature"])
        return df_head

    def plot_graphs(self, input_df, title):
        """Plot precipitation level along the year"""
        Y_value_mean = [0]*len(input_df)
        legend_left_ar = []
        legend_right_ar = []
        x_ticks = [0] * self.MONTHS_IN_YEAR
        x_labels = [0] * self.MONTHS_IN_YEAR

        for i in range(len(input_df)):
            Y_value_mean[i] = np.mean(input_df[i][0]["Precipitation level"])
            if i == 0:
                ax=input_df[i][0].reset_index().plot(x='Day_of_year', y='Precipitation level',
                                    color=input_df[i][1],
                                    title=title)
            else:
                input_df[i][0].reset_index().plot(x='Day_of_year', y='Precipitation level', ax=ax, color=input_df[i][1])

        ax2 = ax.twinx()

        for i in range(len(input_df)):
            ax2.axhline(y=Y_value_mean[i], color=input_df[i][2], linestyle='--')
            ax2.text(0.92, Y_value_mean[i], round(Y_value_mean[i], 2), fontsize=8, va='center', ha="left",
                     bbox=dict(facecolor="w", alpha=0.75), transform=ax2.get_yaxis_transform())
            legend_left_ar.append(f"PL in {input_df[i][3]}")
            legend_right_ar.append(f"Mean PL in {input_df[i][3]}")

        x_ticks[0] = calendar.monthrange(self.YEAR, 1)[1]
        x_labels[0] = calendar.month_name[1]

        for i in range(1, len(x_ticks)):
            x_ticks[i] = x_ticks[i - 1] + calendar.monthrange(self.YEAR, i + 1)[1]
            x_labels[i] = calendar.month_name[i + 1]

        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels, ha='right', rotation='30')
        ax.legend(legend_left_ar, loc="upper left", fontsize=8)
        ax2.legend(legend_right_ar, loc="upper right", fontsize=8)
        ax.set_ylabel('Precipitation level (mm)')
        ax2.set_ylabel('Mean precipitation level (mm)')
        plt.show()


# Import of input databases with max and min temperatures and precipitation level in the coolest
# place (Oymyakon) in the world and in the hottest place (Death Valley)
df_death_valley = pd.read_csv('https://raw.githubusercontent.com/JuliaPakhotina/MIP_data/main/Death_Valley.csv')
df_oymyakon = pd.read_csv('https://raw.githubusercontent.com/JuliaPakhotina/MIP_data/main/Oymyakon.csv')

w_plot = WeatherPlot()
clean_df_death_valley = w_plot.clean_data(df_death_valley)
clean_df_oymyakon = w_plot.clean_data(df_oymyakon)

clean_df_death_valley = clean_df_death_valley.astype(float)
clean_df_oymyakon = clean_df_oymyakon.astype(float)

title = "Precipitation level (PL) in the hottest place in the world (Death Valley, USA) \n and the coldest place in " \
        "the world (Oymyakon, Russia)"

w_plot.plot_graphs([[clean_df_death_valley, 'darkred', 'red', "Death Valley"], [clean_df_oymyakon, 'darkblue', 'blue',
                                                                                "Oymyakon"]], title)




